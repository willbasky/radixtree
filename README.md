# A radix (prefix?) tree parsing library

Takes any old `Foldable` of `Text` values, and provides a parser for any parsers
that have a `CharParsing` instance (from
the [parsers](https://hackage.haskell.org/package/parsers) package, i.e., at
least trifecta, parsec, and attoparsec).

Why this is better than `choice . map text` is speed. That approach becomes
prohibitively slow for "large" corpuses; e.g., for parsing some `Text` from 881
alternatives, the difference is an order of magnitude:

```
Benchmark radixtree-parsing: RUNNING...
benchmarking attoparsec/radix
time                 5.082 μs   (5.070 μs .. 5.096 μs)
                     1.000 R²   (1.000 R² .. 1.000 R²)
mean                 5.095 μs   (5.080 μs .. 5.110 μs)
std dev              49.09 ns   (39.26 ns .. 64.72 ns)
             
benchmarking attoparsec/radix compressed
time                 5.068 μs   (5.058 μs .. 5.077 μs)
                     1.000 R²   (1.000 R² .. 1.000 R²)
mean                 5.079 μs   (5.066 μs .. 5.103 μs)
std dev              55.42 ns   (34.22 ns .. 87.68 ns)
             
benchmarking attoparsec/naiive
time                 69.88 μs   (69.39 μs .. 70.51 μs)
                     1.000 R²   (0.999 R² .. 1.000 R²)
mean                 69.79 μs   (69.53 μs .. 70.23 μs)
std dev              1.056 μs   (811.4 ns .. 1.551 μs)
             
Benchmark radixtree-parsing: FINISH
```

A neat (and experimental) feature of `radixtree` is that it can compress trees
to use *only* the original corpus, as a single `Text` value, and simply store
the array offset/lengths into that original value for each node. See
`compressBy`. The 'CompressedRadixTree' version used for the above benchmark
(using the same corpus) is 254032 bytes large, whereas the ordinary 'RadixTree'
is a relatively rotund 709904 bytes. The source `[Text]` is a relatively slim
69840 bytes (and a `Vector Text` version clocks in at 56952 bytes).
